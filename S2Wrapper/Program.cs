﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using S2Wrapper;
using S2Wrapper.Services;
using Serilog;
using Serilog.Events;
using System;
using System.IO;
using System.Threading.Tasks;

Log.Logger = new LoggerConfiguration().MinimumLevel.Information().MinimumLevel.Override("Microsoft.AspNetCore", LogEventLevel.Information).Enrich.FromLogContext().WriteTo.File("log.txt", rollingInterval: RollingInterval.Day).CreateLogger();

try
{
    Log.Information("Starting S2Wrapper");
    var builder = WebApplication.CreateBuilder(args);
    builder.Logging.AddSerilog(Log.Logger);
    builder.Services.AddSingleton<ISavage2Process, Savage2Process>();
    builder.Services.AddSingleton<IStreamAccessor, StreamAccessor>();
    builder.Services.AddSingleton<IDispatcher, Dispatcher>();
    var app = builder.Build();

    app.Services.GetService<ISavage2Process>().Run();
    await app.RunAsync();
}
catch (Exception ex)
{
    Log.Fatal(ex, "Host terminated unexpectedly");
}
finally
{
    Log.CloseAndFlush();
}