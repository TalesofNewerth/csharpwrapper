﻿using Microsoft.Extensions.Configuration;
using S2Wrapper.Services;
using Serilog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace S2Wrapper
{
    public interface ISavage2Process
    {
        void Run();
    }
    public class Savage2Process : ISavage2Process, IDisposable
    {
        private readonly IConfiguration _config;
        private readonly IStreamAccessor _stream;
        private readonly IDispatcher _dispatcher;
        private readonly Process _process;
        public Savage2Process(IConfiguration config, IStreamAccessor stream, IDispatcher dispatcher)
        {
            _config = config;
            _stream = stream;
            _dispatcher = dispatcher;
            _process = new Process
            {
                StartInfo = new ProcessStartInfo()
                {
                    FileName = _config["BinPath"],
                    Arguments = _config["Arguments"],
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    RedirectStandardInput = true,
                    CreateNoWindow = true,
                }
            };
        }

        public void Dispose()
        {
            _process.Close();
            _process.Dispose();
        }

        public void Run()
        {
            _process.OutputDataReceived += new DataReceivedEventHandler(OutputHandler);
            _process.ErrorDataReceived += new DataReceivedEventHandler(OutputHandler);
            _process.Start();
            _stream.Init(_process.StandardInput);
            _process.BeginOutputReadLine();
            _process.BeginErrorReadLine();
            _process.WaitForExit();
        }

        private void OutputHandler(object sendingProcess, DataReceivedEventArgs outLine)
        {
            var function = _dispatcher.GetRegexes().Select(x => (KeyValuePair<Regex, Action<GroupCollection>>?)x).FirstOrDefault(x => x.Value.Key.IsMatch(outLine.Data));

            Console.WriteLine(outLine.Data);

            if (function == null)
                return;

            function.Value.Value.Invoke(function.Value.Key.Match(outLine.Data).Groups);
        }
    }
}
