﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace S2Wrapper.Services
{
    public interface IDispatcher
    {
        IDictionary<Regex, Action<GroupCollection>> GetRegexes();

        void NotImplemented(GroupCollection args);
    }
    public class Dispatcher : IDispatcher
    {
        private readonly IDictionary<Regex, Action<GroupCollection>> Regexes; 
        public Dispatcher()
        {
            Regexes = new Dictionary<Regex, Action<GroupCollection>>()
            {
                { new Regex(@"SGame: Server Status: Map\((.*?)\) Timestamp\((\d+)\) Active Clients\((\d+)\) Disconnects\((\d+)\) Entities\((\d+)\) Snapshots\((\d+)\)", RegexOptions.Compiled), NotImplemented},
                { new Regex(@"Server Status: Map\((.*?)\) Timestamp\((\d+)\) Active Clients\((\d+)\) Disconnects\((\d+)\) Entities\((\d+)\) Snapshots\((\d+)\)", RegexOptions.Compiled), NotImplemented},
                { new Regex(@"Sv: New client connection: #(\d+), ID: (\d+), (\d+\.\d+\.\d+\.\d+):(\d+)", RegexOptions.Compiled), NotImplemented},
                { new Regex(@"Sv: Client #(\d+) set name to (\S+)", RegexOptions.Compiled), NotImplemented},
                { new Regex(@"Sv: Client #(\d+) is ready to enter the game", RegexOptions.Compiled), NotImplemented},
                { new Regex(@"^K2 Engine start up.*", RegexOptions.Compiled), NotImplemented},
                { new Regex(@"NewGameStarted", RegexOptions.Compiled), NotImplemented},
                { new Regex(@"Sv: (\S+) has connected.", RegexOptions.Compiled), NotImplemented},
                { new Regex(@"Sv: \[(.+?)\] ([^\s]+?): (.*)", RegexOptions.Compiled), NotImplemented},
                { new Regex(@"SGame: Removed client #(\d+)", RegexOptions.Compiled), NotImplemented},
                { new Regex(@"(?:SGame: |Sv: )*?SetGamePhase\(\): (\d+) start: (\d+) length: (\d+) now: (\d+)", RegexOptions.Compiled), NotImplemented},
                { new Regex(@"(?:SGame: |Sv: )*?Client #(\d+) requested to join team: (\d+)", RegexOptions.Compiled), NotImplemented},
                { new Regex(@"Sv: (\S+) has been killed by (\S+)", RegexOptions.Compiled), NotImplemented},
                { new Regex(@"(?:SGame:|Sv:)?.?Client #(\d+) requested change to: (\S+)", RegexOptions.Compiled), NotImplemented},
                { new Regex(@"SGame: (\S+) has resigned as commander.", RegexOptions.Compiled), NotImplemented},
                { new Regex(@".*\d+\.\d+\s{3, 6}", RegexOptions.Compiled), NotImplemented},
                { new Regex(@"Sv: ITEM: Client (\d+) (\S+) (.*)", RegexOptions.Compiled), NotImplemented},
                { new Regex(@"^refresh", RegexOptions.Compiled), NotImplemented},
                { new Regex(@"CLIENT (\d+) is on TEAM (\d+)", RegexOptions.Compiled), NotImplemented},
                { new Regex(@"^SERVER-SIDE client count, Team 1 (\d+), Team 2 (\d+)", RegexOptions.Compiled), NotImplemented},
                { new Regex(@"Sv: Client (\d+) index is (\d+). ACTION: (\S+)", RegexOptions.Compiled), NotImplemented},
                { new Regex(@"^.* #(\d+) .*: (\d+\.\d+\.\d+\.\d+).*\\'(\S+)\'", RegexOptions.Compiled), NotImplemented},
                { new Regex(@"^CLIENT (\d+) is LEVEL (\d+)", RegexOptions.Compiled), NotImplemented},
                { new Regex(@"^STARTTOURNEY", RegexOptions.Compiled), NotImplemented},
                { new Regex(@"^NEXTDUELROUND", RegexOptions.Compiled), NotImplemented},
                { new Regex(@".*MISSING: (\S+)", RegexOptions.Compiled), NotImplemented},
                { new Regex(@"SGame: DUEL: (\d+) defeated (\d+)", RegexOptions.Compiled), NotImplemented},
                { new Regex(@"SGame: DUEL_STARTED (\d+) (\d+) (\d+) (\S+) (\S+)", RegexOptions.Compiled), NotImplemented},
                { new Regex(@"SGame: DUEL_ENDED (\d+) (\d+)", RegexOptions.Compiled), NotImplemented},
                { new Regex(@"SGame: REMOVED PLAYER (\d+) TEAM (\d+)", RegexOptions.Compiled), NotImplemented},
                { new Regex(@"^SERVERVAR: (\S+) is (.*)", RegexOptions.Compiled), NotImplemented},
                { new Regex(@"Sv: HACKCHECK ClientNumber: (\d+), AccountID: (\d+), Hashcheck result: (\S+)", RegexOptions.Compiled), NotImplemented},
                { new Regex(@"SGame: Authenticated server successfully, stats will be recorded this game. Match ID: (\d+), Server ID: (\d+)", RegexOptions.Compiled), NotImplemented},
                { new Regex(@"(?:SGame: |Sv: )*?EVENT (\S+) (\S+) on (\S+) by (\S+) of type (\S+) at (\d+\.\d+) (\d+\.\d+) (\d+\.\d+)", RegexOptions.Compiled), NotImplemented},
                { new Regex(@"Error: CWorld::GetTerrainHeight\(\) - Coordinates are out of bounds", RegexOptions.Compiled), NotImplemented},
                { new Regex(@"(?:SGame: |Sv: )*?SCRIPT Client (\d+) (\S+) with value (.*)", RegexOptions.Compiled), NotImplemented},
            };
        }

        public IDictionary<Regex, Action<GroupCollection>> GetRegexes()
        {
            return Regexes;
        }

        public void NotImplemented(GroupCollection args)
        {

        }
    }
}
