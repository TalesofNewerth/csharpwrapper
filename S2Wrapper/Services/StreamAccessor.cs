﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace S2Wrapper.Services
{
    public interface IStreamAccessor
    {
        ValueTask DisposeAsync();

        void Init(StreamWriter stream);

        Task BroadcastAsync(string args);

        void Broadcast(string args);
    }
    public class StreamAccessor : IStreamAccessor, IAsyncDisposable
    {
        private StreamWriter Stream;

        public void Init(StreamWriter stream)
        {
            Stream = stream;
        }

        public async Task BroadcastAsync(string args)
        {
            await Stream.WriteLineAsync(args);
        }

        public void Broadcast(string args)
        {
            Stream.WriteLine(args);
        }

        public async ValueTask DisposeAsync()
        {
            if (Stream == null)
                return; 

            Stream.Close();
            await Stream.DisposeAsync();
        }
    }
}
